     .oPYo.        .oPYo.                   8                                                .oPYo.          o
     8   `8        8    8                   8                                                8   `8          8
    o8YooP' .oPYo. 8      oPYo. .oPYo. .oPYo8 .oPYo. .oPYo. .oPYo. .oPYo. .oPYo. .oPYo.     o8YooP' .oPYo.  o8P
     8   `b 8oooo8 8   oo 8  `' .oooo8 8    8 8oooo8 Yb..   8    ' 8    8 8    8 8oooo8      8   `b 8    8   8
     8    8 8.     8    8 8     8    8 8    8 8.       'Yb. 8    . 8    8 8    8 8.          8    8 8    8   8
     8    8 `Yooo' `YooP8 8     `YooP8 `YooP' `Yooo' `YooP' `YooP' `YooP' 8YooP' `Yooo'      8oooP' `YooP'   8
    :..:::..:.....::....8 ..:::::.....::.....::.....::.....::.....::.....:8 ....::.....::::::......::.....:::..:
    ::::::::::::::::::::8 ::::::::::::::::::::::::::::::::::::::::::::::::8 ::::::::::::::::::::::::::::::::::::
    ::::::::::::::::::::..::::::::::::::::::::::::::::::::::::::::::::::::..::::::::::::::::::::::::::::::::::::

This bot is intended to fill a special use case that is not addressed on Gradescope. For a single assignment
there may be multiple graders. However functionality is not present for graders to respond to a regrade
request that addresses multiple graders. This program speeds up email coordination between course staff.

## Getting Started

- Use an up-to-date (3.0+) Python instance.
- Clone this repository anywhere using `git clone https://gitlab.com/zachbryant/regradescope.git`
- Run `pip3 install -r requirements.txt` to install python dependencies.
  - You will likely need to install the `mutt` email tool via `sudo apt-get install mutt`. Test this to make sure it's setup correctly.
- Configure `regrader.conf`
  - In the config, "rubric_tas" are items checked/marked off on gradescope. The example happens to go by roman numerals.
- Run `./run.sh` and follow the prompts.
  - You can select multiple classes and assignments through regex. For example, entering 13|14|17 will match all homeworks with those numbers, such as "Homework 13 Code Standard".
  - You'll be able to read through the student's request comment to determine what they need regraded. Many students are unclear about this, so it's recommended to have gradescope open in another tab.
  - The program will automatically search for roman numerals, but it isn't perfect. There is a prompt loop that will allow you to remove/add numerals.
  - At the end, each assignment will correspond to an email to the student with all of the graders (including the maintainer) cc'd. The rest is manual but should be easy if the graders are quick to respond.

## FAQ

- What is data/ready.html?
  - This is a file that holds the html content of the most recent email. You can preview how the email will look before you decide to send it.
- What is data/emailed.json?
  - This is a file that stores the URL of regrades that have already been sent out.
- What is data/regrader.conf?
  - This is the configuration file for regradescope. You can find a template in the templates folder
