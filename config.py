import json


# This class provides a basic helper method for reading the config file.
class Config:

    def __init__(self):
        with open("data/regrader.conf", "r") as config_file:
            self.map = json.loads(config_file.read())
        assert (self.get("maintainers")), "No configured maintainers!"


    def get(self, key, default=None):
        return self.map.get(key, default)
