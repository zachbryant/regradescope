import re
import json
import shutil
import subprocess
from regrade import Regrade
from getpass import getpass
import requests_html as requests
from sty import fg, bg, ef, rs, Rule

color_pink = fg(255, 153, 255)
color_mint = fg(0, 255, 153)
color_sky = fg(102, 204, 255)
color_redorange = fg(255, 102, 102)
color_beehive = fg(255, 204, 102)


class Gradescope:
    def __init__(self, config):
        self.GRADESCOPE_URL = "https://www.gradescope.com/"
        self.GRADESCOPE_LOGIN_URL = self.GRADESCOPE_URL + "login"
        self.GRADESCOPE_ACCOUNT_URL = self.GRADESCOPE_URL + "account"
        self.GRADESCOPE_LOGOUT_URL = self.GRADESCOPE_URL + "logout"
        self.config = config
        # Dictionary of String:List
        self.graders = self.config.get("rubric_tas")
        self.suffix = self.config.get("email_suffix")
        self.course_name = self.config.get("course")
        self.maintainer = self.config.get("maintainers")[0]
        self.maintainer_email = self.maintainer["email"]
        self.maintainer_name = self.maintainer["name"]


    # The overall regrade process.
    def run(self):
        self.login()
        self.get_lectures()
        if self.lectures:
            self.get_assignments()
            if self.assignments:
                self.get_regrade_requests()
                self.send()

    # This method will take selenium to the login page and prompt the user for a password.
    def login(self):
        self.session = requests.HTMLSession()
        username = self.maintainer_email
        request = self.session.get(self.GRADESCOPE_URL)
        authenticity_token = request.html.find(
            'input[name="authenticity_token"]', first=True).attrs['value']
        password = getpass(
            "Gradescope Password for %s: " % (color_sky + username + fg.rs))
        headers = {
            'Origin': 'https://www.gradescope.com',
            'Content-Type': 'application/x-www-form-urlencoded',
            'User-Agent': 'Mozilla/5.0',
            'Referer': 'https://www.gradescope.com/',
        }
        data = {
            'authenticity_token': authenticity_token,
            'session[email]': username,
            'session[password]': password,
            'session[remember_me]': [0, 1],
            'commit': 'Log In',
            'session[remember_me_sso]': 0
        }
        request = self.session.post(
            self.GRADESCOPE_LOGIN_URL, headers=headers, data=data)
        if request.url == self.GRADESCOPE_ACCOUNT_URL:
            print("Welcome %s!" % (color_mint + self.maintainer_name + fg.rs))
        else:
            print(color_redorange + "Login failed" + fg.rs)
            self.login()


    # Gets the list of all finished regrades.
    def get_done(self):
        with open("data/emailed.json", "r") as file:
            return file.read().split()


    # Checks if this assignment regrade has already been addressed.
    def is_done(self, link):
        return link in self.get_done()


    # Set the status of an assignment as done, and don't consider this one again.
    def set_done(self, link):
        done = self.get_done()
        done.append(link)
        done = "\n".join(done)
        with open("data/emailed.json", "w") as file:
            file.write(done)


    # Get all available lectures this semester based on regex.
    # NOTE: IF THE SITE CSS CHANGES THEN THE lecture_selector MUST ALSO CHANGE!
    def get_lectures(self):
        request = self.session.get(self.GRADESCOPE_ACCOUNT_URL)
        lecture_selector = "div.courseList:nth-child(2) a"
        course_boxes = request.html.find(lecture_selector)
        self.lectures = {}
        name_ext_pairs = []
        lecture_list = []
        for box in course_boxes:
            lecture = box.find(".courseBox--shortname", first=True).text
            extension = box.absolute_links.pop()
            pair = (lecture, extension)
            name_ext_pairs.append(tuple(pair))
            lecture_list.append(lecture)

        if not name_ext_pairs:
            print(color_redorange + "No lectures found" + fg.rs)
        else:
            lecture_format = color_beehive + ef.bold
            joiner = fg.white + rs.bold_dim + ",\n\t" + lecture_format
            lecture_list_string = joiner.join(lecture_list)
            print("Lectures: \n\t" + lecture_format + lecture_list_string +
                  fg.rs + rs.bold_dim)
            self.regex = input(
                "Select lectures by regex? (Enter to skip): ").lower()
            for lecture, extension in name_ext_pairs:
                prompt = "Run %s? (y/n) " % (
                    lecture_format + lecture + fg.rs + rs.bold_dim)
                if (not self.regex or re.search(
                        self.regex, lecture.lower())) and "y" in input(prompt):
                    self.lectures[lecture] = extension + "/assignments"


    # Return list of assignments in a particular lecture.
    # NOTE: IF THE SITE CSS CHANGES THEN THE assignment_selector MUST ALSO CHANGE!
    def get_assignments(self):
        self.assignments = {}
        found_assignments = False
        assignment_format = color_mint + ef.bold
        joiner = fg.white + rs.bold_dim + ",\n\t" + assignment_format
        titles = []
        assignment_selector = "#assignments-instructor-table > tbody:nth-child(2) > tr"
        for key, url in self.lectures.items():
            print()
            self.assignments[key] = []
            request = self.session.get(url)
            rows = request.html.find(assignment_selector)
            if not rows:
                print(color_redorange + "No assignments found for %s" % key + fg.rs)
            else:
                for row in rows:
                    links = row.find('a')
                    title = links[0].text
                    titles.append(title)
                title_list_string = assignment_format + joiner.join(
                    titles) + fg.rs + rs.bold_dim
                assignment_key = ef.bold + color_beehive + str(
                    key) + fg.rs + rs.bold_dim
                print("Assignments for %s: \n\t%s" % (assignment_key,
                                                      title_list_string))
                regex = input("Select assignments by regex? (Enter to skip): ")
                for row in rows:
                    links = row.find('a')
                    title = links[0].text
                    prompt = "Process %s? (y/n): " % (
                        assignment_format + title + fg.rs + rs.bold_dim)
                    if (not regex or re.search(
                            regex, title)) and "y" in input(prompt):
                        url = links[1].absolute_links.pop()
                        countElement = links[1].find(".badge", first=True)
                        if countElement:
                            count = int(countElement.text)
                            if count > 0:
                                found_assignments = True
                                a = {
                                    "title": title,
                                    "link": url,
                                    "count": count,
                                    "requests": []
                                }
                                self.assignments[key].append(a)
                                print(
                                    "%s regrade%s pending" %
                                    (ef.bold + color_redorange + str(count) + fg.rs +
                                     rs.bold_dim, "s" if count > 1 else ""))
                                continue
                        print(color_sky + "No regrades for " + title + fg.rs)
            if found_assignments:
                print()
        if not found_assignments:
            print(color_beehive + "No assignments to process" + fg.rs)


    # Get the string comment of a regrade request.
    def get_comment(self, url):
        request = self.session.get(url)
        #Seperate finds because we want the paragraphs of first div.mathLabel element
        paragraphs = request.html.find("#main-content > div", first=True)
        data = json.loads(paragraphs.attrs['data-react-props'])
        comment = data['open_request']['student_comment'].strip().replace(
            '\\n', "<br/>")
        return comment


    # Process regrade requests for the selected lectures/assignments and put them in a map to be emailed later.
    def get_regrade_requests(self):
        row_selector = 'tbody > tr'
        for lecture, assignments in self.assignments.items():
            for assignment in assignments:
                print("Requests for %s" % (ef.bold + color_mint + assignment["title"]
                                           + fg.rs + rs.bold_dim))
                request = self.session.get(assignment["link"])
                rows = request.html.find(row_selector)
                for row in rows:
                    cols = row.find("td")
                    icons = cols[3].find("i.fa", first=True)
                    if not icons:
                        student = cols[0].text
                        link = cols[4].absolute_links.pop()
                        if not self.is_done(link):
                            comment = self.get_comment(link)
                            regrade = Regrade(student, lecture,
                                              assignment["title"], comment,
                                              link)
                            regrade.assign_graders_on_comment(self.graders)
                            assignment["requests"].append(regrade)


    # Return string template of email to be sent.
    def get_template(self):
        template = None
        with open("template/email.html", "r") as email:
            template = email.read()
        return template


    # Format the email template
    def format_template(self, name, student_name, alink, comment):
        template = self.get_template()
        return template % (name, student_name, alink, comment,
                           self.maintainer_email)


    # A helper method for short salutations. Dont't want to say hello x and x and x and x....
    def get_email_salutation(self, regrade):
        name = "All"
        if len(regrade.graders) == 1:
            name = regrade.graders[0]["name"]
        elif len(regrade.graders) == 2:
            names = [regrade.graders[0]["name"], regrade.graders[1]["name"]]
            names.sort()
            name = "%s and %s" % tuple(names)
        return name


    # Loop through completed regrade requests and seek approval to send emails.
    # This method may be tedious but it ensures that if something breaks, nobody loses their inbox.
    def send(self):
        command = 'mutt -e "set content_type=text/html;my_hdr From:%s<%s>" -b %s -s "[%s] Regrade %s %s" %s < data/ready.html'
        for lecture, assignments in self.assignments.items():
            for assignment in assignments:
                for regrade in assignment["requests"]:
                    name = self.get_email_salutation(regrade)
                    comment = regrade.comment.replace("\n", "<br/>")
                    student = regrade.requester
                    msg = self.format_template(name, student, regrade.link,
                                               comment)
                    with open("data/ready.html", "w") as file:
                        file.write(msg)
                    to_addresses = []
                    for grader in regrade.graders:
                        to_addresses.append(grader["id"] + self.suffix)
                    if to_addresses:
                        to_addresses = " ".join(to_addresses)
                        email = self.maintainer_email
                        cmd = command % (self.maintainer_name, email, email,
                                         re.search(self.regex.upper(),
                                                   lecture.upper()).group(0),
                                         str(regrade.assignment), str(student),
                                         to_addresses)
                        print(cmd)
                        status = input(color_redorange +
                                       "Does this command look okay?" + fg.rs +
                                       " (y/n/skip) ")
                        if "y" in status:
                            subprocess.run(cmd, shell=True)
                            self.set_done(regrade.link)
                        elif "s" in status:
                            file_title = "data/needs_attention/%s_%s" % (
                                student, str(regrade.assignment))
                            shutil.copy("data/ready.html",
                                        "%s.html" % file_title)
                            with open("%s.sh" % file_title, "w") as cmd_file:
                                cmd_file.write(cmd)
                                cmd_file.write(
                                    "\necho -n -e '\\n%s' >> emailed.json\n" %
                                    regrade.link)
