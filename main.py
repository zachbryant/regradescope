from sty import fg, bg, ef, rs, Rule
from gs_interface import Gradescope
from config import Config

pink = fg(255, 153, 255)
mint = fg(0, 255, 153)
blue = fg(0, 153, 255)
redorange = fg(255, 102, 102)
beehive = fg(255, 204, 102)

splash = """
 .oPYo.        .oPYo.                   8                                                .oPYo.          o  
 8   `8        8    8                   8                                                8   `8          8  
o8YooP' .oPYo. 8      oPYo. .oPYo. .oPYo8 .oPYo. .oPYo. .oPYo. .oPYo. .oPYo. .oPYo.     o8YooP' .oPYo.  o8P 
 8   `b 8oooo8 8   oo 8  `' .oooo8 8    8 8oooo8 Yb..   8    ' 8    8 8    8 8oooo8      8   `b 8    8   8  
 8    8 8.     8    8 8     8    8 8    8 8.       'Yb. 8    . 8    8 8    8 8.          8    8 8    8   8  
 8    8 `Yooo' `YooP8 8     `YooP8 `YooP' `Yooo' `YooP' `YooP' `YooP' 8YooP' `Yooo'      8oooP' `YooP'   8  
:..:::..:.....::....8 ..:::::.....::.....::.....::.....::.....::.....:8 ....::.....::::::......::.....:::..:
::::::::::::::::::::8 ::::::::::::::::::::::::::::::::::::::::::::::::8 ::::::::::::::::::::::::::::::::::::
::::::::::::::::::::..::::::::::::::::::::::::::::::::::::::::::::::::..::::::::::::::::::::::::::::::::::::
"""
version = "1.2"
git = "https://gitlab.com/zachbryant/regradescope.git"


def main():
    print(mint + splash + fg.rs)
    print(beehive + "Version: " + fg.rs + version)
    print(beehive + "Git Repo: " + fg.rs + git)
    config = Config()
    gs = Gradescope(config)
    gs.run()


if __name__ == "__main__":
    main()