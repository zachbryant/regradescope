import re
from sty import fg, bg, ef, rs, Rule

color_pink = fg(255, 153, 255)
color_mint = fg(0, 255, 153)
color_blue = fg(0, 153, 255)
color_redorange = fg(255, 102, 102)
color_beehive = fg(255, 204, 102)


# This class contains data & helpers directly related to an individual regrade request.
#
# Values:
# requester: the student requesting a regrade.
# assignment: the name of the specific assignment.
# lecture: which class/lecture title they fall under on gradescope.
# link: the direct assignment link.
# comment: the student's comment.
# graders: a list of graders mapping from rubric items to school IDs. Used to send emails to the right people.

class Regrade:
    def __init__(self,
                 requester,
                 lecture,
                 assignment,
                 comment,
                 link,
                 graders=None):
        if graders is None:
            graders = []
        self.requester = requester.strip()
        self.assignment = assignment.strip()
        self.lecture = lecture.strip()
        self.link = link
        self.comment = comment.strip()
        self.graders = graders


    # A mutt-friendly email string to cc graders with.
    def grader_string(self):
        rules = []
        for grader in self.graders:
            rule = grader["rules"]
            if rule not in rules:
                rules.append(rule)
        return ", ".join(rules)


    # Attempt to find the correct graders to cc based on the comment.
    # Hopefully the student actually says which rules they want graded.
    # Will enter a verification loop.
    def assign_graders_on_comment(self, grader_map):
        for rule, grader_list in grader_map.items():
            match = re.search(
                "([^A-Z]|^)%s((\s+)?)((:|-|\.)((\s+)?)[A-Z]?)" % rule.upper(),
                self.comment.upper())
            if match:
                self.graders.extend(grader_list)
        print("\nRequest by %s:\n%s\n" %
              (color_beehive + self.requester + fg.rs,
               color_pink + self.comment.replace("<br/>", "\n") + fg.rs))
        self.check_right_graders(grader_map)


    # Remove graders from the email.
    def remove_graders(self, grader_map, remove):
        for r in remove:
            for gr in grader_map.get(r.upper(), []):
                self.graders.remove(gr)


    # Add graders to the email.
    def insert_graders(self, grader_map, missing):
        for m in missing:
            if grader_map.get(m, None) not in self.graders:
                self.graders.extend(grader_map[m])


    # A method for manually assigning the correct rules. Enters a verification loop.
    def assign_manual_graders(self, grader_map):
        missing = input("Enter rules manually: ").split()
        self.graders = []
        self.insert_graders(grader_map, missing)
        self.check_right_graders(grader_map)


    # Pretty print the rules we found.
    def display_rules(self):
        print(color_beehive + "Regrade rules: %s\n" % (fg.rs + self.grader_string()))


    # Ask the user for the right/wrong graders and clean the grader list accordingly.
    # This is a verification loop for the user. It's easy for muscle memory to enable mistakes.
    def check_right_graders(self, grader_map):
        self.display_rules()
        prompt_right = color_redorange + "Does this look right?" + fg.rs + " (y/n) "
        if not "y" in input(prompt_right):
            missing = input("Missing rules: ").split()
            self.insert_graders(grader_map, missing)
            remove = input("Extra rules: ").split()
            self.remove_graders(grader_map, remove)
            self.display_rules()
            if not "y" in input(prompt_right):
                self.assign_manual_graders(grader_map)


    # Returns a dictionary with all data from this regrade request.
    def get_dict(self):
        return {
            "requester": self.requester,
            "lecture": self.lecture,
            "assignment": self.assignment,
            "comment": self.comment,
            "link": self.link,
            "graders": self.graders
        }


    def __str__(self):
        return str(self.get_dict())


    def __repr__(self):
        return self.__str__()
